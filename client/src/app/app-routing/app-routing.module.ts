import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NoticiasComponent } from '../noticias/noticias.component';
import { NoPageFoundComponent } from '../no-page-found/no-page-found.component';
import { DetalleNoticiaComponent } from '../detalle-noticia/detalle-noticia.component';

const routes: Routes = [
  { path: 'noticias', component: NoticiasComponent},
  { path: 'noticias/:noticiaId', component: DetalleNoticiaComponent },
  {path: '', component: NoticiasComponent},
  {path: '**', component: NoPageFoundComponent},
  ]

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
