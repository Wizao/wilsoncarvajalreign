import { Component, OnInit } from '@angular/core';
import { NoticiasService } from '../services/noticias.service';
import { ActivatedRoute } from '@angular/router';
import { Noticia } from '../interface/noticia';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-detalle-noticia',
  templateUrl: './detalle-noticia.component.html',
  styleUrls: ['./detalle-noticia.component.css']
})
export class DetalleNoticiaComponent implements OnInit {
  
  constructor(private noticiaService: NoticiasService, private route: ActivatedRoute) { }
  noticia: Noticia;
  noticiaId = "";
  ngOnInit(): void {
        this.route.paramMap.subscribe(
      
      params=> {
        this.noticiaId = params.get('noticiaId');
        console.log(params.get('noticiaId'));

    })
    
    this.getNoticia(this.noticiaId).subscribe(resp =>{ 
      
      this.noticia = resp
      console.log(this.noticia);
      
    });
    
    
      
  }

  getNoticia(noticiaId: string) {
   
    return this.noticiaService.getNoticia(noticiaId);
  }
}
