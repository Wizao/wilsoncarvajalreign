export interface Noticia {
    _id: string ;
    parent_id: string ;
    story_url: string ;
    story_title: string ;
    title: string;
    story_id: string ;
    num_comments: string ;
    story_text: string ;
    points: string ;
    author: string ;
    url: string ;
    created_at: Date ;
    comment_text: string ;
    __v: number ;
}
