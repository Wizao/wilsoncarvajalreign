import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoticiasComponent } from './noticias.component';
import {NoticiasService} from '../services/noticias.service';

describe('NoticiasComponent', () => {
  let component: NoticiasComponent;
  let fixture: ComponentFixture<NoticiasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoticiasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticiasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Debería retornar un array de Noticias', () => {
    
    
    const Noticias = component.getAllNoticias();

    expect(Noticias).toBeDefined();
  });


});
