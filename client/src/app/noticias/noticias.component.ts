import { Component, Inject, OnInit } from '@angular/core';
import { Noticia } from '../interface/noticia';
import { NoticiasService } from '../services/noticias.service';

@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.css']
})
export class NoticiasComponent implements OnInit {
  constructor(private noticiaService: NoticiasService) { }
  noticias: Noticia[] = [];
  ngOnInit(): void {
    this.getAllNoticias().subscribe(resp => {
      this.noticias = resp;
    }
    )
  }

  getAllNoticias() {
    return this.noticiaService.getAllNoticias();
  }

  deleteNoticia(noticia: Noticia) {
    return this.noticiaService.deleteNoticia(noticia).subscribe((resp) =>
      (this.noticias = this.noticias.filter((noti) => noti._id! !== noticia._id)));
  }

  onNavigate(noticia: Noticia) {
    if (noticia.story_url != null) {
      window.open(noticia.story_url);
    } else {
      window.open(noticia.url);
    }

  }

}
