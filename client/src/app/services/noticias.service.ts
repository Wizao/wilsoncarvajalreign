import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Noticia } from '../interface/noticia';


@Injectable({
  providedIn: 'root'
})
export class NoticiasService {
private urlApi = 'http://localhost:5600';

  constructor(private http: HttpClient) { 
  }

  getAllNoticias():Observable<Noticia[]>{
    
    return this.http.get<Noticia[]>(this.urlApi+'/noticias');
  }

  getNoticia(noticiaId: String):Observable<Noticia>{
    return this.http.get<Noticia>(this.urlApi+'/noticias/'+noticiaId);
  }

 deleteNoticia(noticia: Noticia): Observable<Noticia>{
    return this.http.delete<Noticia>(this.urlApi+'/noticias/'+noticia._id);
 }
}
