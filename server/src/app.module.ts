import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NoticiasModule } from './noticias/noticias.module';
import {HttpModule} from '@nestjs/axios';

@Module({
  imports: [NoticiasModule, MongooseModule.forRoot(process.env.DB_URI), HttpModule, ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
