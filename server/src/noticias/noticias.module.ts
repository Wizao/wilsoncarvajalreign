import { Module } from '@nestjs/common';
import { NoticiasService } from './noticias.service';
import { NoticiasController } from './noticias.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Noticia, NoticiaSchema } from './schemas/noticias.schemma';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [MongooseModule.forFeature([{ name: Noticia.name, schema: NoticiaSchema }]), HttpModule],
  controllers: [NoticiasController],
  providers: [NoticiasService]
})
export class NoticiasModule {}
