import { Model, ObjectId } from 'mongoose';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Noticia, NoticiaDocument } from './schemas/noticias.schemma';
import { CreateNoticiaDto } from './dto/create-noticia.dto';
import { UpdateNoticiaDto } from './dto/update-noticia.dto';
import { HttpService } from '@nestjs/axios';



@Injectable()
export class NoticiasService {

  constructor(@InjectModel(Noticia.name) private noticiaModel: Model<NoticiaDocument>, private httpService: HttpService) {}

  
  async create(createNoticiaDto: CreateNoticiaDto):Promise<Noticia> {
   
    return new this.noticiaModel(createNoticiaDto).save();
  }

  async createAll(): Promise<Noticia[]> {

    let respuesta = [];

    const migración =  this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs').subscribe((response) => {


    for (const data of response.data.hits) {
      const createNoticiaDto = data;
      console.log(data.story_title);
      respuesta.push(data);
      new this.noticiaModel(createNoticiaDto).save();

     }
     return respuesta;
});
     return respuesta;
  }

  async findAll(): Promise<Noticia[]> {
    return this.noticiaModel.find().exec();
  }

  async findOne(id: string) :Promise<Noticia> {
    const noticia = await this.noticiaModel
    .findById({ _id: id })
    .exec();

  if (!noticia) {
    throw new NotFoundException(`Noticia #${noticia} no encontrada`);
  }

  return noticia;
  }

  update(id: number, updateNoticiaDto: UpdateNoticiaDto) {
    return `This action updates a #${id} noticia`;
  }

  async remove(id: string) {
     const noticia = await this.noticiaModel
    .findByIdAndDelete({ _id: id })
    .exec();

  if (!noticia) {
    throw new NotFoundException(`Noticia #${noticia} no encontrada`);
  }

  return noticia;
  }

  async migracion(createNoticiaDto: CreateNoticiaDto):Promise<Noticia> {
    const createdCat = new this.noticiaModel(createNoticiaDto);
    return createdCat.save();
  }
}
