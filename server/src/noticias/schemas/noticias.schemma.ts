import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type NoticiaDocument = Noticia & Document;

@Schema()
export class Noticia {
  @Prop()
  name: string;
  @Prop()
  comment_text: string;
  @Prop()
  created_at: Date;
  @Prop()@Prop()
  url: string;
  @Prop()
  author: string;
  @Prop()
  points: string;
  @Prop()
  story_text: string;
  @Prop()
  num_comments: string;
  @Prop()
  story_id: string;
  @Prop()
  story_title: string;
  @Prop()
  story_url: string;
  @Prop()
  parent_id: string;
  @Prop()
  title: string;
}

export const NoticiaSchema = SchemaFactory.createForClass(Noticia);